package ru.t1.kravtsov.tm.component;

import ru.t1.kravtsov.tm.api.IBootstrap;
import ru.t1.kravtsov.tm.api.ICommandController;
import ru.t1.kravtsov.tm.api.ICommandRepository;
import ru.t1.kravtsov.tm.api.ICommandService;
import ru.t1.kravtsov.tm.constant.ArgumentConst;
import ru.t1.kravtsov.tm.constant.TerminalConst;
import ru.t1.kravtsov.tm.controller.CommandController;
import ru.t1.kravtsov.tm.repository.CommandRepository;
import ru.t1.kravtsov.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    @Override
    public void run(String[] args) {
        parseArguments(args);
        parseCommands();
    }

    private void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;

        final String arg = args[0];
        parseArgument(arg);
        exit();
    }

    private void parseCommands() {
        commandController.displayWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("\nENTER COMMAND:");
            final String command = scanner.next();
            parseCommand(command);
        }
    }

    private void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;

        switch (arg) {
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            default:
                commandController.displayArgumentError();
                System.exit(1);
        }
    }

    private void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;

        switch (command) {
            case TerminalConst.VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                commandController.displayCommandError();
        }
    }

    private void exit() {
        System.exit(0);
    }

}
