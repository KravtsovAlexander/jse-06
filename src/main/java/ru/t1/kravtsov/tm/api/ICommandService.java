package ru.t1.kravtsov.tm.api;

import ru.t1.kravtsov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
