package ru.t1.kravtsov.tm.api;

public interface IBootstrap {

    void run(String[] args);

}
